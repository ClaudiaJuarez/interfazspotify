# SpotifyApp (app-spotify)

INTERFAZ GRAFICA DE USUARIO DE LA VENTANA PRINCIPAL DE SPOTIFY

El aplicativo consta de la creación de la interfaz gráfica de la ventana principal de spotify, implementado con los
componentes que ofrece el framework de QUASAR.

## Pasos para instalar el framework de Quasar

Para poder ejecutar esta aplicación es necesario tener instalado el framework de QUASAR, de no ser asi se deberan de seguir los
siguientes pasos.

Instalar node js desde la pagina oficial (https://nodejs.org/es/)

Instalar el framework de Quasar desde la consola CMD, mediante el siguientes comando
`npm i -g @quasar/cli`

## Comando para descargar archivos faltantes

Despues de descargar el proyecto desde GitLab, se debe de ejecutar el siguiente comando
`npm install`
para poder crear la carpeta de archivos de los modulos que se indican en el archivo packages.json.

## Correr el proyecto

Para poder correr el proyecto, se debe de teclear el siguiente comando
`quasar dev`
Nota: Este comando se debe de ingresar desde la ruta donde se encuentra el proyecto localmente.

## Ejecución de la aplicación

Al ejecutar el comando antes mencionado que permite correr el proyecto, automaticamente se abrira la aplicación en el navegador
mostrando la interfaz grafica realizada, de no ser asi, se debera ingresar manualmente la dirección o URL que nos proporciona
quasar que normalmente se ejecuta en la dirección (http://localhost:8080).

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).
